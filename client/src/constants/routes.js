export const ROUTES = {
  HOME: "/", // This is the landing page.
  MAIN: "/find-a-counselor",
  CONTACT: "/contact",
  ABOUT: "/about",
  FAQ: "/faq",
  INFO: '/info',
  HISTORY: '/history',
  PROFILE: '/profile',
  ADMIN: '/admin',
  REGISTERUSER: '/registeruser'
};
