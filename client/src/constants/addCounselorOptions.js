export const OPTIONS = [
  {
    category: "Gender",
    list: ["Male", "Female", "Other"],
  },
  {
    category: "Pronouns",
    list: ["He/Him", "She/Her", "They/Them"],
  },
  {
    category: "Specialization",
    list: [
      "ADHD",
      "Addiction",
      "Adoption",
      "Alcohol Use",
      "Alzheimer's",
      "Anger Management",
      "Anxiety",
      "Bipolar Disorder",
      "Domestic Abuse",
      "Obesity",
      "PTSD",
    ],
  },

  {
    category: "Approach",
    list: [
      "Cognitive Behavioural (CBT)",
      "Trauma Focused",
      "Attachment-Based",
      "Somatic",
      "Dialectical",
      "Acceptance and Commitment (ACT)",
      "Psychodynamic",
      "EMDR",
      "Strength-Based",
      "Culturally Sensitive",
    ],
  },
  {
    category: "Credentials",
    list: ["CCC", "CCP", "RCC", "RSW", "PhD"],
  },
  {
    category: "Age",
    list: [
      "Toddlers/Preschoolers (0 to 6)",
      "Children (6 to 10)",
      "Preteens/Tweens (11 to 13)",
      "Adolescents/Teenagers (14 to 19)",
      "Adults",
      "Elders (65+)",
    ],
  },
  {
    category: "Ethnicity",
    list: [
      "Asian",
      "Indigenous",
      "Hispanic and Latino",
      "Black",
      "Hawaiian",
      "Guatemalan",
    ],
  },
];
